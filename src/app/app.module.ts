import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {HttpClientModule} from "@angular/common/http";
import {AuthenticateService} from "./authenticate.service";
import {AuthGuardService} from "./auth-guard.service";
import { LoginComponent } from './login/login.component';
import { BookListComponent } from './books/book-list/book-list.component';
import { CreateBookComponent } from './books/create-book/create-book.component';
import { BookComponent } from './books/book/book.component';
import {AvailabilityPipe} from "./__pipes/availability.pipe";
import { BooksModule } from './books/books.module';
import {HomeComponent} from "./home/home.component";
import {PagenotfoundComponent} from "./pagenotfound/pagenotfound.component";

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    HomeComponent,
    PagenotfoundComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BooksModule
  ],
  providers: [AuthGuardService, AuthenticateService],
  bootstrap: [AppComponent]
})
export class AppModule { }

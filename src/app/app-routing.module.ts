import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import {HomeComponent} from './home/home.component';
import {AuthGuardService} from "./auth-guard.service";
import {PagenotfoundComponent} from './pagenotfound/pagenotfound.component'
import {LoginComponent} from './login/login.component';
import {BookListComponent} from './books/book-list/book-list.component';
import {CreateBookComponent} from './books/create-book/create-book.component';
const routes: Routes = [

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'createBook', component: CreateBookComponent, canActivate: [AuthGuardService] },
  { path: 'books', component: BookListComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: '**', component: PagenotfoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

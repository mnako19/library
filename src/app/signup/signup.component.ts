import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import {AuthenticateService} from "../authenticate.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

users:any;

  constructor(private authService: AuthenticateService,
              private router: Router,
              firestore: AngularFirestore) {
    this.users = firestore.collection('users').valueChanges();
  }


  ngOnInit(): void {
  }
  login() {
    this.users.subscribe(console.log)
    this.authService.setLoggedIn(true);
    this.router.navigate(['/home']);
  }

}

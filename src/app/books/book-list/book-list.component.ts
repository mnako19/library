
import { Component, OnInit } from '@angular/core';
import {Book} from "../../__module/book.model";
import {BooksService} from "../../books.service";
import {ActivatedRoute, Router} from '@angular/router';
import { BookComponent } from '../book/book.component';
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  public books: Book[] = [
    {
      isbn: '0553213695',
      author: 'Franz Kafka',
      title: 'The Metamorphosis ',
      genre: 'Classics',
      year: '2000',
      available: true
    },
    {
      isbn: '0811201880',
      author: 'Jean-Paul Sartre',
      title: 'Nausea',
      genre: 'Philosophy',
      year: '1999',
      available: false
    },
  ]
  constructor(private router: Router,
              private bookService: BooksService) { }


  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe( res => {
      console.log(res)})
    }

  onChangeAvailability(isbn: string) {
    const book = this.books.find(book => book.isbn === isbn);

    if (book !== undefined) {
      book.available = !book.available;
    }
  }

  navigateToBook(book: any) {
    const edit: boolean = book.isbn % 2 === 0 ? true : false;
    this.router.navigate(['/book', book.isbn], {queryParams: {editing: edit}});
  }



}



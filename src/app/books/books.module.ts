import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import { CreateBookComponent } from "./create-book/create-book.component";
import {BookListComponent} from "./book-list/book-list.component";
import {BookComponent} from "./book/book.component";
import {AvailabilityPipe} from "../__pipes/availability.pipe";



@NgModule({
  declarations: [
    BookListComponent,
    BookComponent,
    CreateBookComponent,
    AvailabilityPipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class BooksModule { }





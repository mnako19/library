import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {UserModel} from "../app//__module//user.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private firestore: AngularFirestore,
              private httpClient: HttpClient) { }


  getAllUsers(): Observable<UserModel[]> {
    return this.firestore.collection('users').valueChanges() as Observable<UserModel[]>;
  }

  getUserById(id: number) {

  }
  getMockUsers(): Observable<UserModel[]> {
    return this.httpClient.get('assets/mocks/users.json') as Observable<UserModel[]>;
  }

}


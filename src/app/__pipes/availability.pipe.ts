import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'availability'
})
export class AvailabilityPipe implements PipeTransform {

  transform(isAvailable: unknown, ...args: unknown[]): unknown {
    return isAvailable ? 'Book is available' : 'I am sorry, book is not available';
  }

}
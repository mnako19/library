import { Component, OnInit } from '@angular/core';
import {AuthenticateService} from "../authenticate.service";
import {Router} from "@angular/router";
import {AngularFirestore} from "@angular/fire/firestore";
import {UserServiceService} from "../user-service.service";
import {Observable} from "rxjs";
import {UserModel} from "../__module/user.model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  users:any;
  allUsers: UserModel[] = [];

  constructor(private authService: AuthenticateService,
              private router: Router,
              private userService: UserServiceService) {
  }




  ngOnInit(): void {

    this.userService.getAllUsers().subscribe((response: UserModel[]) => {
      this.allUsers = response;
      console.log(response)
    })

    this.userService.getMockUsers().subscribe( response => {
      console.log(response)
    })
  }



  login(email: string, password: string) {
    this.allUsers.forEach((user: UserModel) => {
      if (user.email === email && user.password === password) {
        this.authService.setLoggedIn(true);
        this.router.navigate(['/home']);
      } else {
        console.error("User does not exist")
      }
    })
  }



}

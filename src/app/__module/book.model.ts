export interface  Book {
    title: string;
    author: string;
    isbn: string;
    genre: string;
    year: string;
    available: boolean;
   
  }
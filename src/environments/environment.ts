// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBu1p8xxC_gOTpXzCCg90iALN1AnA69ViA",
    authDomain: "libaray-7a54f.firebaseapp.com",
    projectId: "libaray-7a54f",
    storageBucket: "libaray-7a54f.appspot.com",
    messagingSenderId: "395719928962",
    appId: "1:395719928962:web:c7bba3f90e299d3647189b",
    measurementId: "G-9L24N9JP1K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
